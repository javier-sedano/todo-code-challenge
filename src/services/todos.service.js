import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import db from './db';

// TODO: replace with axios call to backend.
// In the meanwhile do operations here and return cloned objects.

export default {
  async findTodos(filter, doneFilter) {
    const lowercaseFilter = filter.toLocaleLowerCase();
    const filterFunction = (
      doneFilter === undefined
        ? (todo) => todo.title.toLowerCase().includes(lowercaseFilter)
        : (todo) => todo.title.toLowerCase().includes(lowercaseFilter)
          && todo.done === doneFilter
    );
    return db.todos
      .filter(
        filterFunction,
      ).sort(
        (a, b) => a.title.toLowerCase().localeCompare(b.title.toLowerCase()),
      ).map(
        (todo) => _.cloneDeep(todo),
      );
  },
  async getTodo(id) {
    const found = db.todos
      .find(
        (todo) => todo.id === id,
      );
    return _.cloneDeep(found);
  },
  async updateTodo(updatedTodo) {
    const found = db.todos
      .find(
        (todo) => todo.id === updatedTodo.id,
      );
    if (found !== undefined) {
      found.title = updatedTodo.title;
      found.done = updatedTodo.done;
    }
  },
  async createTodo() {
    const todo = {
      id: uuidv4(),
      title: 'Do something',
      done: false,
    };
    db.todos.push(todo);
    return todo.id;
  },
  async deleteTodo(id) {
    let i;
    for (i = 0; i < db.todos.length; i += 1) {
      if (db.todos[i].id === id) {
        db.todos.splice(i, 1);
        i -= 1;
      }
    }
  },
};
