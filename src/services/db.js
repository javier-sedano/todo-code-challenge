const todos = [
  {
    id: 'e9302b80-4b95-48b0-8d86-e91b46a59c53',
    title: 'Buy groceries',
    done: false,
  },
  {
    id: 'e7f41019-cdfa-40f4-9fef-45d2c66d6563',
    title: 'Code challenge',
    done: false,
  },
  {
    id: 'c53c1b28-275d-497d-a217-7c46126cbca2',
    title: 'Book holidays',
    done: true,
  },
];

export default {
  todos,
};
