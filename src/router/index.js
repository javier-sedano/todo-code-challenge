import { createRouter, createWebHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'Root',
    redirect: '/todos/',
  },
  {
    path: '/todos/',
    name: 'Todos',
    component: () => import(/* webpackChunkName: "todos" */ '@/views/Todos/Todos.vue'),
    children: [
      {
        path: '',
        name: 'TodoNone',
        component: () => import(/* webpackChunkName: "todo_none" */ '@/components/NoneDetails.vue'),
      },
      {
        path: ':id',
        name: 'TodoDetails',
        component: () => import(/* webpackChunkName: "todo_details" */ '@/views/Todos/TodoDetails.vue'),
      },
    ],
  },
  {
    path: '/about/',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import(/* webpackChunkName: "notFound" */ '@/components/NotFound.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
