
## Project setup
```
npm run initDependencies
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
npm run test:unit:watch
npm run test:unit:watchCoverage
```

### Lints and fixes files
```
npm run lint
```
