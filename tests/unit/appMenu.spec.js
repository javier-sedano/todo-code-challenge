import { shallowMount } from '@vue/test-utils';
import AppMenu from '@/AppMenu.vue';

afterEach(() => {
  jest.clearAllMocks();
});

describe('AppMenu.vue', () => {
  const $routerMock = {
    push: jest.fn(),
  };

  it('should create', () => {
    const wrapper = shallowMount(AppMenu, {
      global: {
        stubs: ['router-link'],
        mocks: {
          $router: $routerMock,
        },
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.element).toMatchSnapshot('initial');
  });
});
