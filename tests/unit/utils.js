// eslint-disable-next-line import/prefer-default-export
export function flushPromises() {
  return new Promise((resolve) => setImmediate(resolve));
}
