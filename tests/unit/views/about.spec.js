import { shallowMount } from '@vue/test-utils';
import About from '@/views/About.vue';

afterEach(() => {
  jest.clearAllMocks();
});

describe('About.vue', () => {
  const $routerMock = {
    push: jest.fn(),
  };

  it('should create', () => {
    const wrapper = shallowMount(About, {
      global: {
        stubs: ['font-awesome-icon'],
        mocks: {
          $router: $routerMock,
        },
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.element).toMatchSnapshot('initial');
  });
});
