import { shallowMount } from '@vue/test-utils';
import TodoDetails from '@/views/Todos/TodoDetails.vue';
import { flushPromises } from '../../utils';
import db from '@/services/db';

jest.mock('@/services/db');

afterEach(() => {
  jest.clearAllMocks();
});

describe('TodoDetails.vue', () => {
  const $routerMock = {
    push: jest.fn(),
  };

  async function testCreatedWithLoad() {
    const todos = [
      {
        id: 'e9302b80-4b95-48b0-8d86-e91b46a59c53',
        title: 'Buy groceries',
        done: false,
      },
      {
        id: 'e7f41019-cdfa-40f4-9fef-45d2c66d6563',
        title: 'Code challenge',
        done: false,
      },
      {
        id: 'c53c1b28-275d-497d-a217-7c46126cbca2',
        title: 'Book holidays',
        done: true,
      },
    ];
    const todo = todos[0];

    db.todos = todos;

    const wrapper = shallowMount(TodoDetails, {
      global: {
        stubs: ['font-awesome-icon'],
        mocks: {
          $route: { params: { id: todo.id } },
          $router: $routerMock,
        },
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.todo).toBeUndefined();
    await flushPromises();
    expect(wrapper.vm.todo).toEqual(todo);
    expect(wrapper.emitted().shown.length).toEqual(1);
    expect(wrapper.emitted().shown[0]).toEqual([todo.id]);
    return wrapper;
  }

  it('should create', async () => {
    const wrapper = await testCreatedWithLoad();
    expect(wrapper.element).toMatchSnapshot('initial');
  });

  it('should save', async () => {
    const wrapper = await testCreatedWithLoad();
    const title = 'title';
    wrapper.vm.todo.title = title;
    const done = !wrapper.vm.todo.done;
    wrapper.vm.todo.done = done;
    expect(wrapper.vm.isSaving).toBeFalsy();
    const savePromise = wrapper.vm.save();
    expect(wrapper.vm.isSaving).toBeTruthy();
    await savePromise;
    expect(wrapper.vm.isSaving).toBeFalsy();
    expect(wrapper.emitted().updated.length).toEqual(1);
    const savedTodo = db.todos.find((todo) => todo.id === wrapper.vm.todo.id);
    expect(savedTodo.title).toEqual(title);
    expect(savedTodo.done).toEqual(done);
  });

  it('should ignore saving wrong id', async () => {
    const wrapper = await testCreatedWithLoad();
    const title = 'title';
    wrapper.vm.todo.id = 'nonexisting';
    wrapper.vm.todo.title = title;
    expect(wrapper.vm.isSaving).toBeFalsy();
    const savePromise = wrapper.vm.save();
    expect(wrapper.vm.isSaving).toBeTruthy();
    await savePromise;
    expect(wrapper.vm.isSaving).toBeFalsy();
    expect(wrapper.emitted().updated.length).toEqual(1);
    const savedTodo = db.todos.find((todo) => todo.id === wrapper.vm.todo.id);
    expect(savedTodo).toBeUndefined();
  });

  it('should delete', async () => {
    const wrapper = await testCreatedWithLoad();
    const { id } = wrapper.vm.todo;
    expect(wrapper.vm.isDeleting).toBeFalsy();
    const deletePromise = wrapper.vm.deletee();
    expect(wrapper.vm.isDeleting).toBeTruthy();
    await deletePromise;
    expect(wrapper.vm.isDeleting).toBeFalsy();
    expect(wrapper.emitted().updated.length).toEqual(1);
    const deletedTodo = db.todos.find((todo) => todo.id === id);
    expect(deletedTodo).toBeUndefined();
  });
});
