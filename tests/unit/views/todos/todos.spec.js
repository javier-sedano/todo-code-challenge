import { shallowMount } from '@vue/test-utils';
import Todos from '@/views/Todos/Todos.vue';
import { flushPromises } from '../../utils';
import db from '@/services/db';

jest.mock('@/services/db');

afterEach(() => {
  jest.clearAllMocks();
});

describe('Todos.vue', () => {
  const $routerMock = {
    push: jest.fn(),
  };

  const todoBuy = {
    id: 'e9302b80-4b95-48b0-8d86-e91b46a59c53',
    title: 'Buy groceries',
    done: false,
  };
  const todoCode = {
    id: 'e7f41019-cdfa-40f4-9fef-45d2c66d6563',
    title: 'Code challenge',
    done: false,
  };
  const todoBook = {
    id: 'c53c1b28-275d-497d-a217-7c46126cbca2',
    title: 'Book holidays',
    done: true,
  };

  async function testCreatedWithSearch() {
    const todos = [todoBuy, todoCode, todoBook];
    const expectedTodos = [todoBook, todoBuy, todoCode];

    db.todos = todos;

    const wrapper = shallowMount(Todos, {
      global: {
        stubs: ['router-link', 'router-view', 'font-awesome-icon'],
        mocks: {
          $router: $routerMock,
          $route: '',
        },
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.todos).toEqual([]);
    await flushPromises();
    expect(wrapper.vm.todos).toEqual(expectedTodos);
    return wrapper;
  }

  it('should create', async () => {
    const wrapper = await testCreatedWithSearch();
    expect(wrapper.element).toMatchSnapshot('initial');
  });

  it('should find with filter', async () => {
    const filteredTodos = [
      {
        id: 'e9302b80-4b95-48b0-8d86-e91b46a59c53',
        title: 'Buy groceries',
        done: false,
      },
    ];

    const wrapper = await testCreatedWithSearch();
    const aFilter = 'buy';
    wrapper.vm.filter = aFilter;
    expect(wrapper.vm.isFinding).toBeFalsy();
    const findPromise = wrapper.vm.find();
    expect(wrapper.vm.isFinding).toBeTruthy();
    await findPromise;
    expect(wrapper.vm.isFinding).toBeFalsy();
    expect(wrapper.vm.todos).toEqual(filteredTodos);
  });

  it('should toggle done filter', async () => {
    const wrapper = await testCreatedWithSearch();

    async function toggleAndAssert(expectedInitial, expectedFinal, expectedTodos) {
      expect(wrapper.vm.doneFilter).toEqual(expectedInitial);
      const toggleDoneFilterPromise = wrapper.vm.toggleDoneFilter();
      expect(wrapper.vm.doneFilter).toEqual(expectedFinal);
      expect(wrapper.vm.isFinding).toBeTruthy();
      await toggleDoneFilterPromise;
      expect(wrapper.vm.isFinding).toBeFalsy();
      expect(wrapper.vm.todos).toEqual(expectedTodos);
    }

    await toggleAndAssert(undefined, false, [todoBuy, todoCode]);
    await toggleAndAssert(false, true, [todoBook]);
    await toggleAndAssert(true, undefined, [todoBook, todoBuy, todoCode]);
  });

  it('should toggle small searchPanel', async () => {
    const wrapper = await testCreatedWithSearch();
    expect(wrapper.vm.showSmallSearchPanel).toEqual(false);
    wrapper.vm.toggleShowSmallSearchPanel();
    expect(wrapper.vm.showSmallSearchPanel).toEqual(true);
    wrapper.vm.toggleShowSmallSearchPanel();
    expect(wrapper.vm.showSmallSearchPanel).toEqual(false);
  });

  it('should store selected note and close the small searchPanel', async () => {
    const selectedTodoId = 'e9302b80-4b95-48b0-8d86-e91b46a59c53';
    const wrapper = await testCreatedWithSearch();
    expect(wrapper.vm.selectedTodoId).toBeUndefined();
    wrapper.vm.showSmallSearchPanel = true;
    wrapper.vm.selectTodo(selectedTodoId);
    expect(wrapper.vm.selectedTodoId).toEqual(selectedTodoId);
    expect(wrapper.vm.showSmallSearchPanel).toEqual(false);
  });

  it('should add', async () => {
    const wrapper = await testCreatedWithSearch();
    expect(wrapper.vm.isAdding).toBeFalsy();
    const addPromise = wrapper.vm.add();
    expect(wrapper.vm.isAdding).toBeTruthy();
    await addPromise;
    expect(wrapper.vm.isAdding).toBeFalsy();
    const createdTodo = db.todos.find((todo) => todo.title === 'Do something');
    expect(createdTodo).toBeDefined();
    expect($routerMock.push).toHaveBeenCalledTimes(1);
    expect($routerMock.push).toHaveBeenCalledWith({ name: 'TodoDetails', params: { id: createdTodo.id } });
  });
});
