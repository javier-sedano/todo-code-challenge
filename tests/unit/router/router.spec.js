import router from '@/router';

describe('router index', () => {
  it('should create', () => {
    expect(router).toBeDefined();
  });

  it('should load lazy routes', () => {
    function loadLazyRoutes(routes) {
      routes.forEach((route) => {
        if (typeof route.component === 'function') {
          route.component();
          if (route.children !== undefined) {
            loadLazyRoutes(route.children);
          }
        }
      });
    }
    loadLazyRoutes(router.options.routes);
    expect(router).toBeDefined();
  });
});
