import { shallowMount } from '@vue/test-utils';
import App from '@/App.vue';

afterEach(() => {
  jest.clearAllMocks();
});

describe('App.vue', () => {
  function testCreated() {
    const wrapper = shallowMount(App, {
      global: {
        stubs: ['router-link', 'router-view', 'font-awesome-icon'],
      },
    });
    expect(wrapper).toBeDefined();
    return wrapper;
  }

  it('should create', () => {
    const wrapper = testCreated();
    expect(wrapper.element).toMatchSnapshot('initial');
  });

  it('should toggle small menu', () => {
    const wrapper = testCreated();
    expect(wrapper.vm.menuVisible).toEqual(false);
    wrapper.vm.toggleMenu();
    expect(wrapper.vm.menuVisible).toEqual(true);
    wrapper.vm.toggleMenu();
    expect(wrapper.vm.menuVisible).toEqual(false);
  });
});
