import { shallowMount } from '@vue/test-utils';
import NoneDetails from '@/components/NoneDetails.vue';

describe('NoneDetails.vue', () => {
  function testCreated() {
    const wrapper = shallowMount(NoneDetails);
    expect(wrapper).toBeDefined();
    expect(wrapper.emitted().shown.length).toEqual(1);
    expect(wrapper.emitted().shown[0]).toEqual([undefined]);
    return wrapper;
  }
  it('should create', () => {
    const wrapper = testCreated();
    expect(wrapper.element).toMatchSnapshot('initial');
  });
});
