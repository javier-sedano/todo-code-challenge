import { shallowMount } from '@vue/test-utils';
import NotFound from '@/components/NotFound.vue';

describe('NotFound.vue', () => {
  function testCreated() {
    const wrapper = shallowMount(NotFound);
    expect(wrapper).toBeDefined();
    return wrapper;
  }
  it('should create', () => {
    const wrapper = testCreated();
    expect(wrapper.element).toMatchSnapshot('initial');
  });
});
